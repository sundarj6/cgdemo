﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace SpecFlowDemo
{
    [Binding]
    public class Global_USLaunch_TestSteps
    {
        //IWebDriver driver = new FirefoxDriver();
        IWebDriver driver = new ChromeDriver();
        [Given(@"User launches Browser and enters GC URL")]
        public void GivenUserLaunchesBrowserAndEntersGCURL()
        {
            //Launch Browser
            driver.Navigate().GoToUrl("https://www.capitalgroup.com/");
            driver.Manage().Window.Maximize();
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
        }
        
        [When(@"User Launches Other Global CG page")]
        public void WhenUserLaunchesOtherGlobalCGPage()
        {
            //Launch CC_Global_Site
            //IWebElement element_other_site = driver.FindElement(By.XPath("//*[@id='cmp-button--233010344920048']/button/span"));

           // element_other_site.Click();
            // Thread.Sleep(5);
            
        }
        [Then(@"User is at the Other Global CG page")]
        public void ThenUserIsAtTheOtherGlobalCGPage()
        {
            string Title = driver.Title;
            Console.WriteLine("Title is:" + Title);
            Assert.IsTrue(driver.Title.Contains("Home Page | Capital Group"));
        }
                
        [When(@"User Navigate to USA Persoanl Invement Page")]
        public void WhenUserNavigateToUSAPersoanlInvementPage()
        {
            //Launch CG_US_Site			
            //IWebElement element_US = driver.FindElement(By.LinkText("United States"));
            //element_US.Click();
            string TitleCountryPage = driver.Title;
            //Thread.Sleep(5);
            Console.WriteLine("Title is:" + TitleCountryPage);
            //Thread.Sleep(5);
            //Scroll to top of page
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollTo(0,0);");
            //IWebElement element_other_site2 = driver.FindElement(By.ClassName("//*[@id='cmp-button--233010344920048']/button/span"));
            //element_other_site2.Click();
            //Launch_US_Personal_Investment_Page	
            //System.Threading.Thread.Sleep(5);
                         }
        [Then(@"CG USA Personal Investment Page should display")]
        public void ThenCGUSAPersonalInvestmentPageShouldDisplay()
        {
            //Thread.Sleep(5);
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            //WebElement element_US_investor = driver.FindElement(By.XPath("//span[contains(@class, 'cmp-form-button__title') and text() = 'Investor']"));
            IWebElement element_US_investor;
            //element_US_investor = wait.Until(OpenQA.Selenium.Support.UI.ExpectedConditions.ElementIsVisible(By.XPath("//span[contains(@class, 'cmp-form-button__title') and text() = 'Investor']")));
            element_US_investor = wait.Until(OpenQA.Selenium.Support.UI.ExpectedConditions.ElementIsVisible(By.LinkText("Individual Investor")));
            element_US_investor.Click();
            //Thread.Sleep(60);
            string TitleCountryPage2 = driver.Title;
            Console.WriteLine("Title is:" + TitleCountryPage2);
        }
        [When(@"Login Button should be visible")]
        public void WhenLoginButtonShouldBeVisible()
        {
            bool element_login_link = driver.FindElement(By.LinkText("Login")).Displayed;
            if (element_login_link == true)
            {
                Console.WriteLine("Login Negative Test- Login Button Displayed Passed");
            }
            else
            { Console.WriteLine("Login Negative Test Login Button Displayed Failed"); }
        }
                      
        [Then(@"Close the browser")]
        public void ThenCloseTheBrowser()
        {
            //Browser_Close
            driver.Close();
        }
    }
}
